{%- import '~/variables.j2'|rip as variables -%}
variant: fcos
version: 1.4.0

systemd:
  units:
    - name: container-postgresql.service
      enabled: true
      contents: |
        [Unit]
        Description=Run the database service for matrix
        Wants=network.target
        After=network-online.target pod-matrix.service
        Wants=pod-matrix.service

        [Service]
        Environment=PODMAN_SYSTEMD_UNIT=%n
        EnvironmentFile=/etc/sysconfig/postgres.env
        Restart=on-failure
        TimeoutStopSec=70
        ExecStartPre=/bin/rm -f %t/container-postgresql.pid %t/container-postgresql.ctr-id
        ExecStart=/bin/podman run \
            --conmon-pidfile %t/container-postgresql.pid \
            --cidfile %t/container-postgresql.ctr-id \
            --cgroups=no-conmon -d --replace \
            --rm \
            --read-only \
            --pod=matrix \
            --name=postgres \
            -e POSTGRES_USER=dendrite \
            -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
            -v /var/srv/matrix/postgres-data:/var/lib/postgresql/data:Z \
            -v /var/srv/matrix/postgres-create_db.sh:/docker-entrypoint-initdb.d/20-create_db.sh:ro,z \
            docker.io/library/postgres:11 \
            -c listen_addresses='localhost'
        ExecStop=/usr/bin/podman stop --ignore --cidfile %t/container-postgresql.ctr-id -t 10
        ExecStopPost=/usr/bin/podman rm --ignore -f --cidfile %t/container-postgresql.ctr-id
        PIDFile=%t/container-postgresql.pid
        Type=forking

        [Install]
        WantedBy=multi-user.target

storage:
  directories:
    - path: /var/srv/matrix/postgres-data
      mode: 0700
  files:
    - path: /etc/sysconfig/postgres.env
      contents:
        local: {{ 'postgres.env'|rop|ey }}
      mode: 0640
    - path: /var/srv/matrix/postgres-create_db.sh
      contents:
        local: {{ 'create_db.sh'|rop|ey }}
